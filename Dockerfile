
# BASE IMAGE - python 3.5 stretch
FROM python:3.7-slim-stretch

# SET ARGS
ARG ansible_version=2.8.5
ARG user_uid=1000
ARG user_name=mrmilu
ARG user_password=d0ck3r1z4t3
# SET ENV VARS
ENV ansible_version=${ansible_version} 
ENV user_uid=${user_uid}
ENV user_name=${user_name}
ENV user_password=${user_password}


# CREATE USER
RUN useradd -m -p ${user_password} -u ${user_uid} -U \
    -s /bin/bash ${user_name} -G root

# WORKDIR
WORKDIR /workdir

# https://github.com/phusion/baseimage-docker/issues/58
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections


# UPDATE AND INSTALL NECESSARY STUFF
RUN apt update && \
    apt install -y --no-install-recommends apt-utils && \
    apt install -y openssh-server python-dev gcc libpq-dev sudo

# CREATE NECESSARY DIRS
RUN sudo -u ${user_name} mkdir /home/${user_name}/.ssh && \
    sudo -u ${user_name} mkdir /home/${user_name}/.aws


# UPGRADE PIP
RUN pip3 install --upgrade pip

# INSTALL NEEDED PYTHON LIBS
RUN pip3 install \
        # AWS DEPENDENCIES - ANSIBLE
        boto boto3 botocore awscli \ 
        jmespath \
        ansible==${ansible_version} \ 
        ansible-lint \
        docker PyYAML jsondiff\
        # MYSQL DEPENDENCIES - ANSIBLE
        PyMySQL \
        # POSTGRE DEPENDENCIES - ANSIBLE
        psycopg2 ipaddress



