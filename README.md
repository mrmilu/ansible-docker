# Image info

Ready-to-use ansible with libs installed like docker, mysql, postgre among others, ansible-lint installed as well. Base image uses python3.7-slim-stretch.

# Build instructions
There are some variables that can be set on build, they are self-explanatory (paired with default values):
* ansible_version: 2.8.5
* user_uid: 1000
* user_name: mrmilu
* user_password: d0ck3r1z4t3

The user_xxxx variables are for a system user that will be created on build time.

Command:
```
$~ docker build --build-arg ansible_version=2.8.5 --build-arg user_name=adsanz -f path/to/dockerfile
```
